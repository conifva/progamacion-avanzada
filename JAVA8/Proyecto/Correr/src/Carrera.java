import java.util.Scanner;

public class Carrera {
	
	//public static String name;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String name = null;
		System.out.print("INGRESA TU NOMBRE\n");
		Scanner scanner = new Scanner(System.in);
		name = scanner.nextLine();
		
		if(name.isEmpty()){
			System.out.println("ERROR!\nUSUARIO NO HA INGRESADO UN NOMBRE");
			Carrera.main(args);
		}
		else{
			System.out.println("¡BIENVENIDO " + name + "!");
		}
			
		//Se crean los corredores, la pista y se 
		//agregan los corredores a la pista.
		Corredor corredor1=new Corredor(name);
		Corredor corredor2=new Corredor("PLAYER2");
		Corredor corredor3=new Corredor("PLAYER3");
		
		//OPCIONES
		Metal.seleccionMetal();
		//20 son los metros a recorrer?
		PistaCarrera pista = new PistaCarrera(20);
		pista.setCorredores(corredor1, corredor2,corredor3);
		pista.empezarCarrera();
		
	}

}
