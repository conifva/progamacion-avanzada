package nave_hilos;

public class Naveroja extends Thread{

	public void run() {
		int i=0;
		long ms=0;
		System.out.println("Comienza la Nave Roja");
		while(i<5){
			try {
				ms = (long)(Math.random()*5+1*1000);
				Thread.sleep(ms);
				System.out.println("Nave Roja se mueve a " + ms + " Km/h");
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			i++;
		}
		System.out.println("La Nave Roja llegó a la meta");
	}
}
