package nave_hilos;

public class Estanque {
	
	private int tipo_combustible;
	private double aceleracion;
	
	public void combustible_aleatorio() {
		
		tipo_combustible = (int) ((Math.random()*3)+1);
		System.out.println("-> TIPO DE COMBUSTIBLE");
		
		if (tipo_combustible == 1) {
			System.out.println("Nuclear");
			aceleracion_combustible(1);
		}
		else if (tipo_combustible == 2) {
			System.out.println("Diesel");
			aceleracion_combustible(2);
		}
		else {
			System.out.println("Biodiesel");
		}
	}
	
	public void aceleracion_combustible(int combustible) {
		
		if(combustible == 1) {
			aceleracion = aceleracion*0.02;
			setAceleracion(aceleracion);
		}
		else if(combustible == 2) {
			aceleracion = aceleracion*0.03;
			setAceleracion(aceleracion);
		}
		else {
			aceleracion = aceleracion*0.04;
			setAceleracion(aceleracion);
		}
	}
	
	
	/*
	 * Getters & Setters
	 */
	
	public int getTipo_combustible() {
		return tipo_combustible;
	}

	public void setTipo_combustible(int tipo_combustible) {
		this.tipo_combustible = tipo_combustible;
	}

	//aceleracion
	public double getAceleracion() {
		return aceleracion;
	}

	public void setAceleracion(double aceleracion) {
		this.aceleracion = aceleracion;
	}

	
}
