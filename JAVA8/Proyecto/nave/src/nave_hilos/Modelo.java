package nave_hilos;

import java.util.Scanner;

public class Modelo {

	private int modelo_selec;
	Scanner sc = new Scanner(System.in);
	
	public void modelos() {
	
		System.out.println("-> Seleccione el modelo");
		System.out.println("[1] Deportivo");
		System.out.println("[2] Liviano");
		System.out.println("[3] Cohete");
		
		setModelo_selec(sc.nextInt());
	}

	public int getModelo_selec() {
		return modelo_selec;
	}

	public void setModelo_selec(int modelo_selec) {
		this.modelo_selec = modelo_selec;
	}
		
}
