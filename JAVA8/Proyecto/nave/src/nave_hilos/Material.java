package nave_hilos;

import java.util.Scanner;

public class Material {

	Scanner sc = new Scanner(System.in);
	private int metal;
	
	
	public void fuselaje(){
		
		System.out.println("-> MATERIAL PRINCIPAL");
		System.out.println("[1] ADAMANTIUM");
		System.out.println("[2] VIBRANIUM");
		System.out.println("[3] CARBONADIUM");
		System.out.println("[4] URU");
		System.out.println("[5] PROMETHIUM");
		
		setMetalSeleccionado(sc.nextInt());
	}


	/**
	 * @return the metal
	 */
	public int getMetalSeleccionado() {
		return metal;
	}


	/**
	 * @param metal the metal to set
	 */
	public void setMetalSeleccionado(int metal) {
		this.metal = metal;
	}
	
}
