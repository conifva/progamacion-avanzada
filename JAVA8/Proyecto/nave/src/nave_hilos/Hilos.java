package nave_hilos;

public class Hilos {
	
	public static void main(String[] args) {
	
		Carrera inicio = new Carrera();
		//inicio.void();
		
		// Instancias
		Naveroja HiloNaveroja = new Naveroja();
		Naveverde HiloNaveverde = new Naveverde();
		// Como el hilo de la Naveverde fue implantado con Runnable
		Thread HV = new Thread(HiloNaveverde);
		Navecohete HiloNavecohete = new Navecohete();
		// Iniciar
		HiloNaveroja.start();
		HV.start();
		HiloNavecohete.start();
		
		System.out.println("Comienza la Carrera:");
	}

	
}
