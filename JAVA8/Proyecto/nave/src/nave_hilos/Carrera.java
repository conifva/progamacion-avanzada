package nave_hilos;

import java.util.Scanner;

public class Carrera {

	Scanner sc = new Scanner(System.in);
	//Reset
	public static final String ANSI_RESET = "\u001B[0m";
	//Colores de letra
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";
	  
	
	Carrera(){
	int opcion;
		
		System.out.println("");
		System.out.println(ANSI_CYAN + "*** BIENVENIDO A CARRERA INTERGÁLACTICA ***" + ANSI_RESET);
		System.out.println("SÚMATE A ESTA EXPERIENCIA EXTRAPLANETARIA");
		System.out.println("");
		System.out.println("[1] CONSTUYE TU NAVE");
		System.out.println("[2] SALIR");
		
		opcion = sc.nextInt();
		
		if (opcion == 1) {
			System.out.print("\033[H\033[2J");
			System.out.flush();
			
			System.out.println("\n***CONSTUYE TU NAVE***");
			Pista pista =new Pista();
			pista.llenar_pista();
		}
		else if (opcion == 2) {
			System.exit(0);
		}
		else {
			System.out.println("ERROR! OPCIÓN INVÁLIDA \n");
			System.exit(0);
		}
	
	}
	
}
