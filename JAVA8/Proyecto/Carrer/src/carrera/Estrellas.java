package carrera;
import javax.swing.ImageIcon;
//libreria
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Estrellas extends JFrame{
	
	JLabel lbNaver, lbNavev, lbNavec, lbCarreteraFondo;
	JButton btnInicio;
	
	
	//constructor
	public Estrellas(){
		//metodo super es para sobreescribir en JFrame
		//nombre en ventana
		super("Carrera Galáctica");
		
		//ordenar imagenes en las posiciones q yo quiero
		getContentPane().setLayout(null);
		
		//señalizar labels

		lbCarreteraFondo = new JLabel("CarreteraFondo");
		getContentPane().add(lbCarreteraFondo);//agrega al frame
		lbCarreteraFondo.setIcon(new ImageIcon(getClass().getResource("estrellas.gif"))); //imagen
		//2 primero numeros son en frame, 2 ultimos tamaño imagen
		lbCarreteraFondo.setBounds(100, 0, 500, 500);
		
		lbNaver = new JLabel("Naver");
		//getContentPane().add(lbNaver);
		lbCarreteraFondo.add(lbNaver);
		lbNaver.setIcon(new ImageIcon(getClass().getResource("roja_opt.png")));
		lbNaver.setBounds(0, 50, 100, 100);
		
		lbNavev = new JLabel("Navev");
		//getContentPane().add(lbNavev);
		lbCarreteraFondo.add(lbNavev);
		lbNavev.setIcon(new ImageIcon(getClass().getResource("verde_opt.png")));
		lbNavev.setBounds(0, 200, 100, 100);
		
		lbNavec = new JLabel("Navec");
		//getContentPane().add(lbNavec);
		lbCarreteraFondo.add(lbNavec);
		lbNavec.setIcon(new ImageIcon(getClass().getResource("cohete_opt.png")));
		lbNavec.setBounds(0, 350, 100, 100);
		
		
		//Boton inicio
		btnInicio = new JButton("Inicio");
		getContentPane().add(btnInicio);
		btnInicio.setBounds(0, 0, 100, 50);
		
		
		//Operacion de salida, o se ejecutaria infinitamente
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		//posicion inicial, ancho y alto ventana
		setBounds(0, 0, 600, 500);
		//visibilidad ventana
		setVisible(true);
		setResizable(false);
	}

}
