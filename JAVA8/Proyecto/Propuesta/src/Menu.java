import java.util.Scanner;

public class Menu {
	
	private static final String ANSI_CYAN = "\u001B[36m";
	private static final String ANSI_RED = "\u001B[31m";
	Scanner sc = new Scanner(System.in);
	private static final String ANSI_RESET = "\u001B[0m";

	//Constructor
	Menu(){
		
		System.out.println("");
		System.out.println("* * * * * * * * * * * * * * * * * * * * ");
		System.out.println(" * * * * * * * * * * * * * * * * * * * *");
		System.out.println("* * * * * * * * * * * * * * * * * * * * ");
		System.out.println(" * * * * * * * * * * * * * * * * * * * *\n");

		System.out.println( "  ------------------------------------");
		System.out.println(ANSI_CYAN + "      SUPER CARRERA INTERGÁLACTICA      " + ANSI_RESET);
		System.out.println( "  ------------------------------------" );
		System.out.println("SÚMATE A ESTA EXPERIENCIA EXTRAPLANETARIA");
		System.out.println("¿ESTÁS LISTO?");		
		
	}
	
	void menuPrincipal() {
		System.out.println( " \n         [1] CONSTUYE TU NAVE\n         [2] SALIR");
		int opcion = sc.nextInt();
		
		//Opciones
		if (opcion == 1) {
			System.out.print("\033[H\033[2J");
			System.out.flush();
			
			System.out.println( "  ------------------------------------");
			System.out.println(ANSI_CYAN + "      CONSTUYE TU NAVE      " + ANSI_RESET);
			System.out.println( "  ------------------------------------" );

			//Fuselaje
			Cuerpo selec = new Cuerpo();
			selec.seleccionMetal();
			
		}
		else if (opcion == 2) {
			System.exit(0);
		}
		else {
			System.out.println(ANSI_RED + "ERROR! OPCIÓN INVÁLIDA \n" + ANSI_RESET);
			
			System.exit(0);
		}
	
	}


}