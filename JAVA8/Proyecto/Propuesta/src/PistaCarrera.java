import java.util.logging.Level;
import java.util.logging.Logger;

public class PistaCarrera {

	private Corredor c1;
	private Corredor c2;
	private Corredor c3;
	private int meta;
	private boolean hayGanador=false;

	public PistaCarrera(int meta) {
        this.meta = meta;
    }
	
	public int getMeta() {
        return meta;
    }
	
	//Recibe los corredores y les informa a
	//estos en que pista estan
	public void setCorredores(Corredor c1, Corredor c2, Corredor c3) {
	    c1.setPista(this);
	    c2.setPista(this);
	    c3.setPista(this);
	    this.c1 = c1;
	    this.c2 = c2;
	    this.c3 = c3;
	}
	
	//Comienza la carrera y no termina hasta que
	//todos los corredores finalizan (con join())
	public void empezarCarrera() {
	    try {
	        c1.start();
	        c2.start();
	        c3.start();

	        c1.join();
	        c2.join();
	        c3.join();
	        System.out.println("Carrera finalizada");
	    } catch (InterruptedException ex) {
	        Logger.getLogger(PistaCarrera.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	
	//Y, lo mas importante, permite a los corredores cruzar la meta. 
	//Y aqui es donde es necesario el synchronized para forzar a 
	//que solo un corredor cruce la meta al mismo tiempo.
	public synchronized void cruzarMeta(Corredor corredor) {
        if(!hayGanador) {
            hayGanador = true;
            System.out.println("Corredor " + corredor.getNombre()+ " es el ganador");
        }
    }
}