import java.util.Scanner;

public class Main {

	private static Scanner scanner;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String name;
		System.out.print("INGRESA TU NOMBRE\n");
		scanner = new Scanner(System.in);
		name = scanner.nextLine();
		
		if(name.isEmpty()){
			System.out.println("ERROR!\nUSUARIO NO HA INGRESADO UN NOMBRE");
			//Carrera.main(args);
		}
		else{
			System.out.println("¡BIENVENIDO/A " + name + "!");
			
			//Se crean los corredores
			Corredor corredor1=new Corredor(name);
			Corredor corredor2=new Corredor("PLAYER2");
			Corredor corredor3=new Corredor("PLAYER3");
			
			//Menu
			Menu mostrar = new Menu();
			mostrar.menuPrincipal();
			
			//Se crean la pista y se agregan los corredores
			//a la pista.
			PistaCarrera pista = new PistaCarrera(20);
			pista.setCorredores(corredor1, corredor2,corredor3);
	//		estanque.setCorredores(corredor1, corredor2,corredor3);
			
			
			//llama a inicia la carrera
			pista.empezarCarrera();
		}
		
		
	}

}
