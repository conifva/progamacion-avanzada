import java.util.Scanner;

public class Cuerpo {

	private static final String ANSI_RED = "\u001B[31m";
	Scanner sc = new Scanner(System.in);
	private static final String ANSI_RESET = "\u001B[0m";
	private int metal; 
	
	Cuerpo(){
		System.out.print("\033[H\033[2J");
		System.out.flush();
		
	}
	
	public void seleccionMetal() {
		
		System.out.println("-> MATERIAL PRINCIPAL");
		System.out.println("[1] ADAMANTIUM");
		System.out.println("[2] VIBRANIUM");
		System.out.println("[3] CARBONADIUM");
		System.out.println("[4] URU");
		System.out.println("[5] PROMETHIUM");
		
		metal = sc.nextInt();
		
		if(metal<=5) {
			//combustible y esas cosas
			Nave nave = new Nave();
			nave.armarNave();
		}
		else {
			System.out.println(ANSI_RED + "ERROR! OPCIÓN INVÁLIDA \n");
			System.out.println("INTÉNTELO NUEVAMENTE O REINICIE EL PROGRAMA" + ANSI_RESET);
			seleccionMetal();
		}
		
	
	}
	
/*	
	public int getMetal() {
		return metal;
	}
	public void setMetal(int metal) {
		this.metal = metal;
	}
*/
	
}
