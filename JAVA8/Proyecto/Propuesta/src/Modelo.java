import java.util.Scanner;

public class Modelo {
	
	Scanner sc = new Scanner(System.in); 
	private int modelo;
	
	public void seleccionarModelo() {
		
		System.out.println("-> TIṔO DE NAVE");
		System.out.println("[1] ASALTO");
		System.out.println("[2] LANZADERA");
		System.out.println("[3] EXPLORACIÓN");
		
		setModelo(sc.nextInt());
	}

	/**
	 * @return the modelo
	 */
	public int getModelo() {
		return modelo;
	}

	/**
	 * @param modelo the modelo to set
	 */
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}

}
