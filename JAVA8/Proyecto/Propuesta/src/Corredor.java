import java.util.logging.Level;
import java.util.logging.Logger;

//Corredor es independiente de la cantidad de participantes
class Corredor extends Thread {

    private int distancia;
    private String nombre;
    private PistaCarrera pista;
    
    //inicia carrera con su nombre y posicion 0m
    public Corredor(String nombre) {
        this.distancia = 0;
        this.nombre = nombre;
        
    }

    @Override
    //Cuando comienza la carrera
    public void run() {
    	System.out.println("COMIENZA [" + nombre + "]");
        while (true) {
            int random = (int) Math.floor(Math.random() * (1 - (10 + 1)) + (10));
            distancia += random;
            System.out.println("[" + nombre + "] Ditancia = " + distancia + " m");
            
            //Si cruza la meta de la pista, le informa a esta ultima que la cruzo 
            //y finaliza su tarea (break al while(true))
            if(distancia > pista.getMeta()) {
                pista.cruzarMeta(this);
                break;
            }
            try {
                sleep(800);
            } catch (InterruptedException ex) {
                Logger.getLogger(Corredor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("[" + nombre + "] llegó a la meta");
    }

    public int getDistanciaRecorrida() {
        return this.distancia;
    }

    public String getNombre() {
        return nombre;
    }

    //Guarda una referencia a la pista en la que compite
    void setPista(PistaCarrera pista) {
        this.pista = pista;
    }
    
}