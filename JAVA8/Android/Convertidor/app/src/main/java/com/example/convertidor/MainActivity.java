package com.example.convertidor;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //PASO0 EN "arrays.xml"
    //PASO1
    //arreglo con denominacion de monedas
    //llanar un spinner con un arrey adapter
    final String[] datos = new String [] {"DOLAR", "PESO CHILENO"};

    //PASO2
    //VARIABLE QUE MANEJA CONTROLES
    //una variable para cada control
    private Spinner spinner_MonedaActual;
    private Spinner spinner_MonedaCambio;
    private EditText editText_ValorCambio;
    private TextView textView_Resultado;

    //PASO3
    //Definir factores de conversion, basado en:
    //1 dolar ---> 680 pesos chile
    //1 peso chile ---> 0.0015 dolar aprox
    final private double factorDolarPeso = 680;
    final private double factorPesoDolar = 0.0015;


    //opcoin para capturar el spiner:
    //--> final Spinner spinner = (Spinner) findViewById(R.id.spinner);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //PASO4
        //ArrayAdapter
        //agregar los elementos del array dato al spinner spinner_MonedaActual
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,datos);

        spinner_MonedaActual = (Spinner) findViewById(R.id.spinner_MonedaActual);
        spinner_MonedaActual.setAdapter(adaptador);
    }

    //PASO5
    //agregar evento click, se enlaza con el boton
    public void button_Convertir(View view) {

        //PASO6
        spinner_MonedaActual = (Spinner) findViewById(R.id.spinner_MonedaActual);
        //Obtener los controles
        //los controles nos informan que elementos de moneda estan seleccionados
        spinner_MonedaCambio = (Spinner) findViewById(R.id.spinner_MonedaCambio);
        //obtener el control ValorCambio
        // el control contiene la informacion del valor de cambio
        editText_ValorCambio = (EditText) findViewById(R.id.editText_ValorCambio);
        //Obtener textView_Resultado
        //este textView permite enviar mensajes al usuario en la interfaz
        textView_Resultado = (TextView) findViewById(R.id.textView_ValorCambio);

        //PASO7
        //spinner_MonedaActual.getSelectedItem()
        //obtine el elemento seleccionado del spinner
        String monedaActual = spinner_MonedaActual.getSelectedItem().toString();
        //lo mismo para spinner_MonedaCambio
        String monedaCambio = spinner_MonedaCambio.getSelectedItem().toString();

        //PASO8
        //obtenemos el valor a cambiar
        //Double.parseDouble([STRING]) permite convertir un String a Double
        double valorCambio = Double.parseDouble(editText_ValorCambio.getText().toString());
        //procesarConversion,  metodo con la logica para procesar la conversion, recibe 3 parametros
        double resultado = procesarConversion(monedaActual, monedaCambio, valorCambio);

        //PASO10
        //si procesarCambio devuelve un valor > 0,
        // el cambio fue procesado correctamente
        if (resultado > 0) {
            //con textView_Resultado.setText()  le informa al usuario el resultado de la conversion
            //String.format("... %12.5fs%...", val1, val2)
            //%5.2f corresponde a val1 - una variable double/float de 12 decimales enteros 5 decimales
            textView_Resultado.setText(String.format("Al convertir %12.5fs%, resulta en %12.5fs%", valorCambio, monedaActual, resultado, monedaCambio));
            //editText_ValorCambio.setText("")
            //borrar cualquier valor del EditText valorCambio
            editText_ValorCambio.setText("");

        }
        //si procesarCambio devuelve un valor <= 0,
        //informar al usuario que ocurrio un problema en la conversion
        else{
            textView_Resultado.setText(String.format("Error"));
            Toast.makeText(MainActivity.this, "Las opciones no se relacionan con un factor de conversión", Toast.LENGTH_SHORT).show();

        }
    }

    //PASO9
    //metodo con la logica para las conversiones
    //recibe 3 parametros monedaActual, monedaCambio, valorCambio
    private double procesarConversion(String monedaActual, String monedaCambio, double valorCambio){
        //PASO11
        //Implementar el metodo
        double resultadoConversion = 0;

        //Evaluar monedaActual
        // opciones posibles: que sea dolar o pesos chilenos
        switch (monedaActual){
            case "DOLAR":
                if(monedaCambio.equals("PESO CHILENO")){
                    resultadoConversion = valorCambio * factorDolarPeso;
                }
                if (monedaCambio.equals("DOLAR")){
                    resultadoConversion = valorCambio;
                }
                break;
            case "PESO CHILENO":
                if (monedaCambio.equals("DOLAR")){
                    resultadoConversion = valorCambio * factorPesoDolar;
                }
                if(monedaCambio.equals("PESO CHILENO")) {
                    resultadoConversion = valorCambio;
                }
                break;
        }
        return resultadoConversion;

    }
}
