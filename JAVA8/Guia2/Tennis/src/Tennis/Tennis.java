package Tennis;
import java.util.Random;


public class Tennis {

	// static porque los set tienen varios juegos
	static boolean jugarJuego(String j1, String j2) {
		
		Random rnd = new Random();
		int marcadorJug1 = 0;
		int marcadorJug2 = 0;
		String puntosJug1 = "0";
		String puntosJug2 = "0";
		boolean ganador = true;
		boolean finJuego = false;
		
		while (finJuego != true) {
			//Golpes jugadores
			int randomGolpe = rnd.nextInt(2);
			
			// Vacío para empezar a escribir
			String marcadorPuntos = "";
			
			// Golpe (ganador) Jugador1
			if (randomGolpe == 0) {
				marcadorJug1 = marcadorJug1 + 1;
				// Resultado juego
				if ((marcadorJug1 > 3) && (Math.abs(marcadorJug1 - marcadorJug2) > 1)) {
					System.out.println();
					System.out.println(j1 + " gana el juego");
					System.out.println();
					marcadorJug1 = 0;
					marcadorJug2 = 0;
					ganador = true;
					finJuego = true;
				}
			}
			// Golpe (ganador) Jugador2
			if (randomGolpe == 1) {
				marcadorJug2 = marcadorJug2 + 1;
				// Resultado juego
				if ((marcadorJug2 > 3) && (Math.abs(marcadorJug2 - marcadorJug1) > 1)) {
					System.out.println();
					System.out.println(j2 + " gana el juego");
					System.out.println();
					marcadorJug1 = 0;
					marcadorJug2 = 0;
					ganador = false;
					finJuego = true;
				}
			}
			// Imprimir puntaje
			// Jugador1
			if (marcadorJug1 == 1) {
				puntosJug1 = "15";
				marcadorPuntos = puntosJug1 + " - " + puntosJug2;
			}
			if (marcadorJug1 == 2) {
				puntosJug1 = "30";
				marcadorPuntos = puntosJug1 + " - " + puntosJug2;
			}
			if (marcadorJug1 == 3) {
				puntosJug1 = "40";
				marcadorPuntos = puntosJug1 + " - " + puntosJug2;
			}
			// Jugador2
			if (marcadorJug2 == 1) {
				puntosJug2 = "15";
				marcadorPuntos = puntosJug1 + " - " + puntosJug2;
			}
			if (marcadorJug2 == 2) {
				puntosJug2 = "30";
				marcadorPuntos = puntosJug1 + " - " + puntosJug2;
			}
			if (marcadorJug2 == 3) {
				puntosJug2 = "40";
				marcadorPuntos = puntosJug1 + " - " + puntosJug2;
			}
			// Casos posibles
			if (marcadorJug1 > 3 && marcadorJug2 > 3 && marcadorJug1 == marcadorJug2) {
				marcadorPuntos = "deuce";
			}
			if (marcadorJug1 > 3 && marcadorJug1 > marcadorJug2) {
				puntosJug1 = "";
				puntosJug2 = "";
				marcadorPuntos = "ventaja " + j1;
			}
			if (marcadorJug2 > 3 && marcadorJug2 > marcadorJug1) {
				puntosJug1 = "";
				puntosJug2 = "";
				marcadorPuntos = "ventaja " + j2;
			}
			
			System.out.println(marcadorPuntos);
		}
		return ganador;
	}
	
	// Sets---> termina partido
	static void jugarSet(float sets, String j1, String j2) {
		
		int juegosJug1 = 0;
		int juegosJug2 = 0;
		int setJug1 = 0;
		int setJug2 = 0;
		float set1 = (sets/2);
		double set = set1 + 0.5;
		boolean finPartido = false;
		
		while (!finPartido) {
			boolean resultadoJuego = jugarJuego(j1, j2);
			// Gana jugador 1 Nadal
			if (resultadoJuego) {
				juegosJug1 = juegosJug1 + 1;
			}
			// Gana jugador 2 Djokovic
			else {
				juegosJug2 = juegosJug2 + 1;
			}
			// Iguales a 6
			if (juegosJug1 == 6 && juegosJug2 == 6) {
				System.out.println("Tie Break");
				// TO-DO:
				// COMENZAR PARTE INTERACTIVA
			}
			
			/*
			 * Imprimir último set y resultado del partido 
			 * resultado = (sets ganado por perdedor / sets ganado por perdedor)
			 */
			
			// Ganador: NADAL (j1)
			if (juegosJug1 >= 6 && (Math.abs(juegosJug1 - juegosJug2) > 1) || juegosJug1 == 7 && juegosJug2 == 6) {
				setJug1 = setJug1 + 1;
				// Last set
				System.out.println(j1 + " gana el set por " + juegosJug1 + " a " + juegosJug2);
				System.out.println();
				juegosJug1 = 0;
				juegosJug2 = 0;
				// Resultado Partido
				if (setJug1 == set && setJug1 > setJug2) {
					System.out.println(j1 + " gana el partido por " + setJug1 + " sets a " + setJug2);
					finPartido = true;
				}
			}
			// Ganador: DJOKOVIC (j2)
			if (juegosJug2 >= 6 && (Math.abs(juegosJug2 - juegosJug1) > 1) || juegosJug2 == 7 && juegosJug1 == 6) {
				setJug2 = setJug2 + 1;
				// Last set
				System.out.println(j2 + " gana el set por " + juegosJug2 + " a " + juegosJug1);
				System.out.println();
				juegosJug1 = 0;
				juegosJug2 = 0;
				// Resultado Partido
				if (setJug2 == set && setJug2 > setJug1) {
					System.out.println(j2 + " gana el partido por " + setJug2 + " sets a " + setJug1);
					finPartido = true;
				}
				
			}
			
		}
		
	}

	
}
