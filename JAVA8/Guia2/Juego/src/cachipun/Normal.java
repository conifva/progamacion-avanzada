package cachipun;

public class Normal {
	private Usuario usuario;
	private Computadora computadora;
	private int usuarioPuntos;
	private int computadoraPuntos;
	

	public enum Move {
		ROCA, PAPEL, TIJERAS;

		/**
		 * Compara esta jugada con otra para determinar 
		 * un empate, triunfo o derrota.
		 * 
		 * otherMove es la jugada a comparar
		 * @return 1 si esta jugada le gana a la otra,
		 * @return -1 se esta jugada pierde frente a la otra
		 * @return 0 si las jugadas empatan
		 */
        
		public int compareMoves(Move otherMove) {
			// Empate
			if (this == otherMove)
				return 0;
			// Casos para ganar
			switch (this) {
			case ROCA:
				return (otherMove == TIJERAS ? 1 : -1);
			case PAPEL:
				return (otherMove == ROCA ? 1 : -1);
			case TIJERAS:
				return (otherMove == PAPEL ? 1 : -1);
			}
			// Algún otro caso
			// Esto no deberia pasar
			return 0;
		}
	}
	
	public void startGame() {
		
		usuario = new Usuario();
		computadora = new Computadora();
		usuarioPuntos = 0;
		computadoraPuntos = 0;
		//System.out.println("ROCA, PAPEL, TIJERAS!");
		
		
		// Obtener jugadas
		Move usuarioMove = usuario.getMove();
		Move computadoraMove = computadora.getMove();
		System.out.println("\nUsuario seleccionó " + usuarioMove + ".");
		System.out.println("Computadora seleccionó " + computadoraMove + ".\n");
		
		// Comparar jugadas y determinar ganador
		int compareMoves = usuarioMove.compareMoves(computadoraMove);
		
		switch (compareMoves) {
		case 0: // Empate
			//System.out.println( compareMoves );

			System.out.println("Empate!");
			break;
		case 1: // Gana Usuario
			//System.out.println( compareMoves );

			System.out.println(usuarioMove + " le gana a " + computadoraMove + ". Ganaste!");
			setUsuarioPuntos(getUsuarioPuntos() + 1);
			break;
		case -1: // Gana Computadora
			//System.out.println( compareMoves );

			System.out.println(computadoraMove + " le gana a " + usuarioMove + ". Perdiste.");
			setComputadoraPuntos(getComputadoraPuntos() + 1);
		break;
		}
		
		// Preguntar al usuario si jugar nuevamente
		if (usuario.playAgain()) {
			System.out.println();
			startGame();
		}
		else {
			System.out.println("Adios");
		}
	}

	public int getUsuarioPuntos() {
		return usuarioPuntos;
	}

	public void setUsuarioPuntos(int usuarioPuntos) {
		this.usuarioPuntos = usuarioPuntos;
	}

	public int getComputadoraPuntos() {
		return computadoraPuntos;
	}

	public void setComputadoraPuntos(int computadoraPuntos) {
		this.computadoraPuntos = computadoraPuntos;
	}

	
}
