package cachipun;
import java.util.Scanner;


public class Main {
	
	public static int opcion;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Bienvenido a Piedra, papel o tijera");
		System.out.println("----Modos de juegos----");
		System.out.println("Juego Normal  [1]");
		System.out.println("Juego Doble   [2] ");
		opcion = sc.nextInt();
		
		if (opcion == 1) {
			System.out.println("Seleccionó jugar con una mano.\n");
			Normal game = new Normal();
			game.startGame();
		}
		else if (opcion == 2) {
			System.out.println("Seleccionó jugar con ambas manos.\n");
			Doble game = new Doble();
			game.startGame();
		}
		else {
			System.out.println("Opción invalida. Adios");

		}

		sc.close();
	}

	
}
