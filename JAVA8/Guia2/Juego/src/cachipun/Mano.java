package cachipun;
import cachipun.Normal.Move;


public class Mano {
	private int Izquierda;
	private int Derecha;
	
	public int getIzquierda() {
		return Izquierda;
	}
	public void setIzquierda(int izquierda) {
		Izquierda = izquierda;
	}
	public int getDerecha() {
		return Derecha;
	}
	public void setDerecha(int derecha) {
		Derecha = derecha;
	}
	
	
	/*
	 * MOVIMEIENTOS CON MANO PRUEBA
	 */
	private Usuario usuario;
	private Computadora computadora;

	public int getUsuarioPuntos() {
		return usuarioPuntos;
	}
	public void setUsuarioPuntos(int usuarioPuntos) {
		this.usuarioPuntos = usuarioPuntos;
	}
	public int getComputadoraPuntos() {
		return computadoraPuntos;
	}
	public void setComputadoraPuntos(int computadoraPuntos) {
		this.computadoraPuntos = computadoraPuntos;
	}
	public int getCantidadDeJuegos() {
		return cantidadDeJuegos;
	}
	public void setCantidadDeJuegos(int cantidadDeJuegos) {
		this.cantidadDeJuegos = cantidadDeJuegos;
	}
	
	private int usuarioPuntos;
	private int computadoraPuntos;
	private int cantidadDeJuegos;
	
	
	public void jugarMano() {
		
		// Obtener jugadas
		Move usuarioMove = usuario.getMove();
		Move computadoraMove = computadora.getMove();
		System.out.println("\nUsuario seleccionó " + usuarioMove + ".");
		System.out.println("Computadora seleccionó " + computadoraMove + ".\n");
		
		// Comparar jugadas y determinar ganador
		int compareMoves = usuarioMove.compareMoves(computadoraMove);
		
		switch (compareMoves) {
		case 0: // Empate
			//System.out.println( compareMoves );
			System.out.println("Ambos seleccionaron el mismo movimiento.");	
			System.out.println("Empate en esta mano");
			break;
		case 1: // Gana Usuario
			//System.out.println( compareMoves );

			System.out.println(usuarioMove + " le gana a " + computadoraMove + ". Ganaste esta mano.");
			setUsuarioPuntos(getUsuarioPuntos() + 1);
			usuarioPuntos++;
			break;
		case -1: // Gana Computadora
			//System.out.println( compareMoves );
			System.out.println(computadoraMove + " le gana a " + usuarioMove + ". Perdiste esta mano.");
			setComputadoraPuntos(getComputadoraPuntos() + 1);
			computadoraPuntos++;
		break;
		
		}
		//cantidadDeJuegos++;

		
	}

}
