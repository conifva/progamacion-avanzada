package cachipun;
import java.util.Scanner;

import cachipun.Normal.Move;


public class Usuario {
	private Scanner inputScanner;
	
	public Usuario() {
		inputScanner = new Scanner(System.in);
	}
	
	public Move getMove() {
		// Solicitar usuario
		System.out.print("Roca, papel o tijeras?\n");
		System.out.println("Roca   [R]");
		System.out.println("Papel  [P]");
		System.out.println("Tijera [T]");
		
		// Obtener entrada
		String usuarioInput = inputScanner.nextLine();
		usuarioInput = usuarioInput.toUpperCase();
		char firstLetter = usuarioInput.charAt(0);
		if (firstLetter == 'R' || firstLetter == 'P' || firstLetter == 'T') {
			// Usuario ha ingresado un dato válido
			switch (firstLetter) {
			case 'R':
				return Move.ROCA;
			case 'P':
				return Move.PAPEL;
			case 'T':
				return Move.TIJERAS;
			}
		}
		
		// Usuario ingreso dato inválido.
		// Solicitar nuevamente.
		System.out.print("Movimiento Inválido\n");
		return getMove();
	}
	// Preguntar al usuario si desea jugar nuevamente
	public boolean playAgain() {
		System.out.print("Quieres jugar de nuevo?\n");
		System.out.print("Presione S/s para volver a jugar\n");
		String usuarioInput = inputScanner.nextLine();
		usuarioInput = usuarioInput.toUpperCase();
		return usuarioInput.charAt(0) == 'S';
	}

	
}
