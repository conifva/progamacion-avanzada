package cachipun;
import cachipun.Normal.Move;

public class Doble {
	private Usuario usuario;
	private Computadora computadora;
	private int usuarioPuntos;
	private int computadoraPuntos;
	private int cantidadDeJuegos;	
	
	
	public void startGame() {
		usuario = new Usuario();
		computadora = new Computadora();

		/*Mano derecha = new Mano();
		 *Mano izquierda = new Mano();
		*/

		// Inicio en 0
		usuarioPuntos = 0;
		computadoraPuntos = 0;
		cantidadDeJuegos = 0;
		
		// Primer juego
		System.out.print("[MANO DERECHA]\n");
		Mano game = new Mano();
		game.jugarMano();
		cantidadDeJuegos++;
		// Segundo juego
		System.out.print("[MANO IZQUIERDA]\n");
		game.jugarMano();
		cantidadDeJuegos++;
		printGameStats();

		// Preguntar al usuario si jugar nuevamente
		if (usuario.playAgain()) {
			System.out.println();
			startGame();
		}
		else {
			// Salir
			System.out.println("Adios");
		}
	}	
	
	private void printGameStats() {
		int wins = usuarioPuntos;
		int losses = computadoraPuntos;
		int ties = cantidadDeJuegos - usuarioPuntos - computadoraPuntos;
		
		// Línea
		System.out.print("+");
		printDashes(68);
		System.out.println("+");
		
		// Imprimir títulos
		System.out.printf("|  %6s  |  %6s  |  %6s  |  %12s  |\n",
				"VICTORIAS", "DERROTAS", "EMPATES", "JUEGOS");

		// Línea
		System.out.print("|");
		printDashes(10);
		System.out.print("+");
		printDashes(10);
		System.out.print("+");
		printDashes(10);
		System.out.print("+");
		printDashes(16);
		System.out.print("+");
		printDashes(18);
		System.out.println("|");
		
		// Imprimir valores
		System.out.printf("|  %6d  |  %6d  |  %6d  |  %12d  |\n",
				wins, losses, ties, cantidadDeJuegos);
		
		// Línea
		System.out.print("+");
		printDashes(68);
		System.out.println("+");
		
	}
	
	
	private void printDashes(int numberOfDashes) {
		for (int i = 0; i < numberOfDashes; i++) {
			System.out.print("-");
		}
	}


	
	
	//Set y Get
	public int getUsuarioPuntos() {
		return usuarioPuntos;
	}

	public void setUsuarioPuntos(int usuarioPuntos) {
		this.usuarioPuntos = usuarioPuntos;
	}

	public int getComputadoraPuntos() {
		return computadoraPuntos;
	}

	public void setComputadoraPuntos(int computadoraPuntos) {
		this.computadoraPuntos = computadoraPuntos;
	}

	public int getCantidadDeJuegos() {
		return cantidadDeJuegos;
	}

	public void setCantidadDeJuegos(int cantidadDeJuegos) {
		this.cantidadDeJuegos = cantidadDeJuegos;
	}
	
	
}
