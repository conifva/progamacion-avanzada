#!/bin/bash

#DIA=`date +%d`
#MES=`date +%m`
#AO=`date +%Y`

#segun parametros (0,1,+)
#NO parametro
if [ $# -le 0 ]; then
	echo "no ingreso ningun parametro, la salida por defecto es:"
	#mostrar calendario
	cal
# 1 parametro
elif [ $# -eq 1 ]; then
	#parametro valido (-s || -l)
	#parametro1 -eq -l || --long
	if [[ $1 == "-l" || $1 == "--long" ]]; then
		echo "Hoy es el `date +%d` del mes `date +%m` del año `date +%Y`"
		exit 0
	#parametro1 -eq -s || --short
	elif [[ $1 == "-s" || $1 == "--short" ]]; then
		echo "`date +%d`/`date +%m`/`date +%Y`"
		exit 0
	else
		echo "opcion incorrecta. solo se acepta el parametro -s y -l"
		exit 1
	fi
#parametro > 1
else
	echo "solo se permite un parametro"
	exit 1
fi 
