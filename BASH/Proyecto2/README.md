# NOTAS

Este programa que permite visualizar en formato SVG los datos identificados como "ATOM" y "HETATM" en archivos con formato PDB que esten registrados como "Protein" (categoría "Macromolecular Type") según la base de datos "https://icb.utalca.cl/~fduran/bd-pdb.txt".

# Obtención del Programa
Clonar repositorio y ejecutar "proyecto1.sh" dentro de la carpeta "Proyecto2" como se muestra a continuación:

```
bash proyecto1.sh
```

# Prerrequisitos
- Sistema operativo Linux (creado y probado en la versión 16.04)
- Conexión a Internet (descarga de archivos)
- Paquete Graphviz. En caso de no contar con este paquete puede descargarlos a través de su terminal como se muestra a continuación:

```
sudo apt-get install graphviz
sudo apt-get update
```

# Objetivo
Ofrecer al usuario un programa en base a Bash Script que genera imagenes (formato SVG) a partir de la información de un archivo descargado desde el sitio Protein Data Bank.

# Controles
Teclado.

# Sobre el código
El programa se compone por 2 Bash Script y varios archivos awk. El primero en ejecutarse es "proyecto1.sh", el que se divide en tres partes; un menú, el análisis de la la existencia de la base de datos y la cantidad de parámetros ingresados, y el análisis de un parámetro correcto por parte del usuario correspondiente a una ID existente en la base de datos.

Al momento de ejecutar el programa aparecera un menú donde el usuario cuenta con 3 opciones; 1) Buscar un archivo tipo pdb en la base de datos para ser descargado y generar un gráfico acorde a los datos de este, 2) mostrar las instrucciones a modo de una breve introducción a el programa en ejecución y 3) salir y volver a la terminal. En caso de seleccionar la primera alternativa se solicitará al usuario el ingreso de una ID correspondiente a los códigos registrados en el sitio Protein Data Bank, se aceptan únicamente UN parámetro de tipo alfanumerico, en caso de no ingresarlo se mostraran distintos tipos de errores informando al usuario y solicitando nuevamente la ID. También se corrobora la existencia de la base de datos que se empleará la cual puede consultar en el siguiente sitio: "https://icb.utalca.cl/~fduran/bd-pdb.txt". Si el usuario no cunta con la base de datos esta se descargará automaticante, en caso contrario se informa se informará a través de la consola que no se cuenta con conexión a Internet.

Cuando se inserte una ID reconocible en la base de datos se informa sobre su coincidencia y se procederá a su descarga si es que esta macromolecula corresponde al tipo "Proteína", en caso contario se informa que no será posible realizar la descarga. Si el usuario no cuenta con conexión a Internet se le enviará nuevamente un mensaje con el error y solicitará volver a intentarlo más tarde. Las proteínas descargadas se almacenarán en la carpeta identificada como "PDB_Downloads" con su ID caracteristica y en formato pdb.

A continuación, se realiza el análisis del archivo pdb en el script "proyecto1-20.sh" donde se genera un archivo de texto que contiene a grandes rasgos la información necesaria para realizar la representación grafica de la proteína en cuestión. Este archivo de texto (puedes encontrarlo el la carpeta de descargas con el nombre "atom_ID.txt", en donde ID hace referencia al código de la proteína) es procesado por "pdb_a_dot.awk" utilizando un conjunto de herramientas de software para el diseño de diagramas definido en el lenguaje descriptivo DOT (Graphviz). El archivo en formato dot también se encuentra en la carpetas de descargas en caso de que el usuario desee visualizarlo y utiliza la misma denominación que el archivo de texto, en otras palabras, poseen el mismo nombre pero son formatos y contenido distinto.

Luego, se realiza una conversión del archivo en formato dot a uno de imagen (formato SVG). La generación de la imagen puede tomar un poco de tiempo dependiendo de la cantidad de datos (tamaño molecular) que se analizan, por lo que se recomienda esperar un par de minutos y no detener el programa. Al igual que con los otros archivos generados la representación grafica de la macromolecula se encuentra en la carpeta de descargas ("PDB_Downloads") con su ID correspondiente en formato SVG. En caso de que el usuario presente problemas para abrir la imagen si no posse un visualizador para este formato se recomienda utilizar su navegador de preferencia.

En caso de que este diagrama proteico ya se encontrara en la carpeta correspondiente se le informa al usuario su nombre y ubicación, además de desplegar un menú secundario son las siguientes opciones: 1) generar gráficos de iones y ligandos a partir de una proteina previamente descargada, 2) mostrar información sobre los gráficos a generar de la opción anterior, 3) retornar al menú principal (anterior) y 4) salir, cerrar el programa y volver a la terminal. En caso de seleccionar la primera alternativa se muestran las carpetas correspondientes a las proteínas descargadas previamente por el usuario y se solicita el ingreso del ID de una de estas para comenzar a procesar la información. Si no se ingresa una ID correspondiente se redirecciona al menú principal. Una vez ingresada la ID se genera un archivo de texto que contiene la información necesaria para realizar la representación grafica de iones y ligandos en cuestión, correspondiente a los elementos "HETATM" del archivo pdb (omitiendo aquellos que contengan "HOH"(agua)), este archivo de texto (puedes encontrarlo el la carpeta de descargas con el nombre "hetatm_ID.txt", en donde ID hace referencia al código de la proteína) se procesado por "centros.awk" que entrega los centros geometricos de los ligandos. Posteriormente, se solicita en ingreso de una distancia en angstrom a la que desea visualizar ligandos e iones, esta ha de ser un valor positivo.

Finalmente, el archivo "hetatm_centros_ID_a.txt" es analizado por "distancia.awk" que evalua los iones y ligandos que se encuentren dentro del rango ingresado anteriormente para ser tranformados a archivos .dot y posteriormente a varios archivos en formato SVG dentro de la carpeta con el nombre de la distancia ingresada.

# Interpretación de Resultados

- Diagrama de proteína

Como ya se mencionó anteriormenta la finalidad de este programa es la visualización grafica de macromoleculas proteicas, identificandose por tanto los aminoácidos que componen sus cadenas y los respectivos elementos de los residuos. Cabe aclarar que al momentos de referirnos a residuos se hace alución a los aminoácidos detectados.

Los aminoácidos que se encuentran unidos mediante flechas indican que pertenecen a una misma cadena aminoacídica y cada residuo tiene señalado su composición, en elementos, indicados por medio de círculos. Si la proteína cuenta con más de uan cadena, como es el caso de la mayoría, se añaden las demas cadenas debajo de los residuos de la Cadena A.

En esta ocasión se tomó la decisión de mostrar cada uno de los aminoácidos presentes en cada cadena, mostrando así la extensa cantidad de los monomeros que las componen y no causar mal entendidos para el usuario en cuanto a el volumen de componentes proteicos. Por ejemplo, si en la Cadena A de una proteína desconocida se tiene conocimiento de que esta compuesta por 13 aminoácidos sería trivial que se "repitieran" algunos, pero sin embago no estar en la misma posición.

Otro punto a tener en cuenta es que los datos recopilados de los archivos descargados desde Protein Data Bank se encuentran ordenados según estos fueron identificados por distintos trabajos de analisis de sustancias, por lo que de igual forma su orden de identificación fue uno de los parametros que se tomó en cuenta a la hora de realizar la gráfica. En consecuencia, el orden correcto de la interpretación esquematizada es en una diagonal ascendente de izquierda a derecha comenzando, primero, por los residuos de la Cadena A y luego continuando con las demás cadenas debajo de esta. 

- Diagrama de iones y ligandos

---

# Errores del programa
- ADVERTENCIA: No modificar los nombres de los elementos, carpetas u otros que se descarguen. Por ejemplo: la carpeta "PDB_Downloads" en donde se almacenan tanto los archivos en formato SVG, PDB y TXT. El cambio de nombre de estos ficheros imposibilitara el correcto funcionamiento del programa.

# Autor
Constanza Valenzuela
