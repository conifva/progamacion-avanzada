#!/bin/bash

codigo="$1"
CARPETA="./PDB_Downloads/"
CARPETA_ID="./$codigo"
cd "$CARPETA"
cd "$CARPETA_ID"

# Analisis $ID_a.pdb, ATOM
echo "       -----------Procesando Datos----------       "
echo -e "Puede tardar unos minutos\n"

# Verificar existencia de txt en la carpeta ID
if [[ ! -f  "atom_$1".txt ]]; then
    grep ^"ATOM" "$codigo".pdb > "atom_$1".txt
    # .txt contiene lo necesario para empezar el analisis, como: 
    # atom(1), elemento(3), aa(4), cadena(5), numero aa(6), x, y, z
    cat "$codigo".pdb |grep ^ATOM |awk '{print (substr($0,0,17) " " substr($0,18,39))}' > "atom_$1".txt
    echo
    # cadena, aa
    ##awk '{print $4, $3}' "atom_$1".txt | sort | uniq > "prueba_aa$1".dot
fi

# Verificar si existe dot en carpeta ID
if [[ ! -f "$codigo".dot ]]; then
    # Copiar archivo txt fuera de la carpeta para que sea analizado por awk
    cp "atom_$1".txt $PWD/../../
    cd $PWD/../../
    # Transformar txt a dot luego del analisis por awk
    cat "atom_$1".txt | awk -f pdb_a_dot.awk >> "$codigo".dot
    # Eliminar archivo txt copiado
    rm "atom_$1".txt
fi

sleep 2

if [[ ! -f "$codigo".svg ]]; then

    echo "       -----------Generando Imagen----------       "
    echo "Puede tardar unos minutos dependiendo del tamaño"
    echo -e "de la macromolecula.\n"
    # Tranformar dot a formato svg
    dot "$codigo".dot -o "$codigo".svg -Tsvg -Gcharset=utf8
    mv "$codigo".svg "$codigo".dot $CARPETA$CARPETA_ID
    echo "       -----------Imagen generada con éxito----------      "

else

    echo "       -----------Imagen anteriormente generada----------      "
fi

echo -e "Nombre del archivo: $codigo.svg\n"
echo "Ingrese a la carpeta con el nombre $CARPETA_ID en "
echo -e "$CARPETA para ver el gráfico de sus aminoácidos.\n"
sleep 3


# [PARTE 3]
# opcion3 1 main2
function ligandos(){


    echo "Próteinas disponibles para generar gráficos a partir"
    echo -e "de iones y ligandos:\n"
    ls $PWD
    read -p "Ingrese ID:" ID
    ID_a=$(echo $ID |tr ‘[a-z]’ ‘[A-Z]’)
    echo
    
    # Verificar existencia de carpeta ingresar y escribir txt
    if [ -d "$ID_a" ]; then
        echo -e "El directorio ${ID_a} existe, comenzando analisis.\n"
        cd $ID_a

    # Directorio no existe
    else
        echo "El directorio ${ID_a} no existe."
        echo -e "Se redicctionará al menú principal.\n"
        cd $PWD/..
        sleep 2
        bash proyecto1.sh
        exit 0
    fi

    echo "       -----------Procesando Datos----------       "
    echo -e "Puede tardar unos minutos\n"
    sleep 2

    # Verificar existencia de txt en la carpeta ID
    if [[ ! -f  "hetatm_$ID_a".txt ]]; then
        # gerera txt y borrar HOH
        grep ^"HETATM" "$ID_a".pdb |egrep -v "HOH" |awk '{print (substr($0,0,7) " " substr($0,18,5) " " substr($0,23,34))}' > "hetatm_$ID_a".txt
        mv "hetatm_$ID_a".txt $PWD/../..
        cd $PWD/../..
        cat "hetatm_$ID_a".txt |awk -f centros.awk > "hetatm_centros_$ID_a".txt
        mv "hetatm_$ID_a".txt $CARPETA$CARPETA_ID
        # Volver a la carpeta de la imagen para q se cree la carpeta de la distancia
        cd $CARPETA$CARPETA_ID

    fi

    echo "Ingrese distancia en Angstrom a la que desea"
    echo "visualizar ligandos e iones"
    read -p "-->>Ingrese distancia:" distancia
    echo -e "Distancia ingresada: $distancia Å​\n"

    if ! [[ "$distancia" =~ ^[0-9\.]+$ ]]; then
        echo "ERROR"
        echo "La distancia debe ser un número positivos"
        echo "en caso de ser decimal estan permitidos"
        echo "los puntos."
        exit 1
    fi

    # Carpeta distancia
    if [[ ! -d "$distancia-$ID_a" ]]; then
        mkdir $distancia-$ID_a

    else
        echo -e "Carpeta creada con anterioridad\n"
    fi

    cd $distancia-$ID_a

    # Existe txt con interacciones 
    if [[ ! -f  "interacciones$distancia".txt ]]; then
        cd $PWD/..
        cp "atom_$ID_a".txt $PWD/../../
        cd $PWD/../..
        awk -v limit=$distancia -f distancias.awk "hetatm_centros_$ID_a".txt "atom_$ID_a".txt >> "interacciones$distancia".txt
        rm "atom_$ID_a".txt
        mv "hetatm_centros_$ID_a".txt $CARPETA$CARPETA_ID
        mv "interacciones$distancia".txt $CARPETA$CARPETA_ID$distancia-$ID_a
        cd $CARPETA$CARPETA_ID$distancia-$ID_a
    fi

    if [[ ! -f  *.dot ]]; then
        cp "interacciones$distancia".txt $PWD/../../..
        cd $PWD/..
        cp "atom_$ID_a".txt $PWD/../../
        cd $PWD/../..
        
        # Con los datos obtenidos, se crea un archivo.dot
        cat "interacciones$distancia".txt | awk -f ligando.awk "interacciones$distancia".txt "atom_$ID_a".txt > "$ID_a".dot
        # NOTA PERSONAL: Revisar ese dot
        
        # Borrar y mover archivos
        rm "atom_$ID_a".txt
        mv "$ID_a".dot $CARPETA$CARPETA_ID$distancia-$ID_a
        mv "interacciones$distancia".txt $CARPETA$CARPETA_ID$distancia-$ID_a
        cd $CARPETA$CARPETA_ID$distancia-$ID_a
    fi

    if [[ ! -f *.svg ]]; then

        echo "       -----------Generando Imagenes----------       "
        echo -e "Puede tardar unos minutos dependiendo de la cantidad\n"
        # Tranformar dot a svg
        # Recorrer archivos .dot y luego genera su imagen
            #for i in *.dot; do
            #    if [[ ! -f $i.svg ]]; then
            #        dot -Tsvg -o $i.svg $i
            #    fi
            #done
        echo -e "       -----------Imagenes generadas con éxito----------      \n"
    else

        echo -e "       -----------Imagenes anteriormente generadas----------      \n"
    fi

    echo "Ingrese a la carpeta $distancia-$ID_a en "
    echo -e "$CARPETA_ID para ver los gráficos.\n"
    echo "Adiós"
    exit 0    
}

# [PARTE 2]
# Luego del gráfico de la proteína, ir a menu2
function main2(){

    echo -e "       -----------Menú Secundario-----------       \n"
    echo "¿Qué desea hacer a continuación?"
    echo "1.- Generar gráficos de iones y ligandos"
    echo "2.- Información sobre gráficos"
    echo "3.- Volver a menú principal (anterior)"
    echo -e "4.- Salir\n"

    read opcion3

    if [[ $opcion3 == "1" ]]; then

        test=$(basename "$PWD")
        test2=$(basename "$CARPETA_ID")
        
        #estoy en la carpeta de la proteina
        if [[ $test == $test2 ]]; then
            #echo "prot descargada antes"
            cd $PWD/..
            ligandos
        else
            #echo "descargue una proteína nueva"
            cd $CARPETA
            # Enviar a otra funcion para analisis HET
            ligandos
        fi


    elif [[ $opcion3 == "2" ]]; then

        echo -e "       -----------INFORMACION-----------       \n"
        echo "Los gráficos de iones y ligandos se generaran "
        echo "únicamente si existe una carpeta con el nombre"
        echo -e "de la proteína solicitada.\n"
        echo "Para que se realicen correctamente los diagramas"
        echo "es vital que no se eliminen los archivos anteri-"
        echo "ormente generados por el programa. En caso de "
        echo "no se encuentren disponibles se informará al u-"
        echo "suario y será necesario que los descargue nueva-"
        echo -e "mente a traves del menú principal.\n"
        echo "Los resultados se archivaran en la carpeta iden-"
        echo "tificada con la distancia ingresada por el usua-"
        echo "rio dentro del fichero con el nombre de la pro-"
        echo "teína en 'PDB_Downloads/'. Las imagenes tendrán"
        echo -e "formato SVG.\n"
        echo "Para abrir la imagen SVG y no contar con visua-"
        echo "lizadores de este formato utilice su navegador "
        echo -e "de preferencia.\n"
        echo -e "¿Desea volver al menú secundario? S/N\n"

        read opcion4
        case $opcion4 in
            s|S)
            echo "Usted pulsó la opción SI"
            clear
            main2
            ;;
            n|N)
            echo "Usted pulsó la opción NO"
            sleep 2
            echo "Fin del programa"
            echo "Adiós"
            exit 0
            ;;
            *)
            echo "Opción invalida"
            echo "Adiós"
            exit 0
            ;;
        esac

    elif [[ $opcion3 == "3" ]]; then

        test3=$(basename "$PWD")

        if [[ $test3 == "Proyecto2" ]]; then
            #echo "descargue una prot nueva"
            bash proyecto1.sh
        else
            #echo "descargue una prot antigua"
            cd $PWD/../..
            bash proyecto1.sh
        fi

    elif [[ $opcion3 == "4" ]]; then
        echo "Adiós"
        exit 0

    else
        echo "Opción inválida"
        sleep 1
        clear
        main2
    fi
}

main2