BEGIN{
    # Contador en 1 utilizado como iterador
    contador=1
}

# Función calcula la distancia entre dos átomos
function cal_distancia(aa, ligando) {

    # Se calcula la diferencia de distancia xyz y se eleva al
    # cuadrado
    x = (aa[X] - ligando[X]) ^ 2
    y = (aa[Y] - ligando[Y]) ^ 2
    z = (aa[Z] - ligando[Z]) ^ 2

    # Distancia raiz de coodenadas xyz (valores sumados),
    # como son vectores se calcula el modulo
    dis_final = sqrt ( x + y + z )

    return dis_final
}

# Como se procesan dos archivos, se condiciona para que 
# comience en el primer archivo (ligandos)

# NR es la linea actual acumulada (todas las lineas de ambos archivos)
# FNR es la linea actual del archivo actual
FNR==NR{

    # Solo usa filas que comiencen con "CENTROS:"
    if ($1 == "CENTROS:"){

        #Se obtienen coordenadas xyz, además del cadena y cadena
        lig_info[contador][nombre] = $2
        lig_info[contador][cadena] = $3
        ligando[contador][X] = $4
        ligando[contador][Y] = $5
        ligando[contador][Z] = $6
        contador++;
    }
next
}

# Cuando deje de cumplirse la condición, se procesará el archivo
# de los ATOM, en este caso las coordenadas de los residuos y átomos
{
    # ID única para cada átomo
    id = $3$4$5
    aa[id][X] = $6
    aa[id][Y] = $7
    aa[id][Z] = $8

}

END{

    # Recorrer ambos arreglos de datos obtenidos de ambos
    # archivos, midiendo la distancia entre centros geométricos de
    # cada ligando y los átomos que conforman cada aminoacido
    for (i in aa){
        for (j in ligando){

            dis_final = cal_distancia(aa[i], ligando[j])

            if (dis_final <= limit){

                # Se agregan las interacciones encontradas al final del
                # archivo de ligandos
                print "INTERACCION " lig_info[j][nombre] lig_info[j][cadena] " -> " i
            }
        }
    }
}
