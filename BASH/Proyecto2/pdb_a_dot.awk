BEGIN{

    # Auxiliar para imprimir " 
    # Inicia gráfico
    x="\""
    print "digraph G {"
}

{
    actual=$4$5$6

    # Revisar si aa actual es igual al anterior
    if(actual != anterior){

        # Distintos entonces se grafica
        print ""
        print "node [shape=tripleoctagon]"
        print actual " [label = "x$4x"]"

        # Misma cadena se unen
        if ($5 == cadena_anterior){
            print actual " -> " anterior "[label = Cadena"$5"]"
        }

        print ""
        print "node [shape=circle]"
    }

    # Se crea nodo por cada elemento diferenciando aminoacidos con el mismo
    # nombre pero con diferente número de residuo (ambos de una misma cadena)
    print $3$5$6 " [label = "x$3x"]"
    print $3$5$6 " -> " actual "[arrowhead = vee]"

    # Comparación de lineas
    anterior=actual
    cadena_anterior=$5
}

END{
    
    # Cerrar el gráfico
    print "}"
}
