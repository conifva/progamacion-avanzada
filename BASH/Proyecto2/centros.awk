BEGIN {
    # Establecer patron
    suma_coord_x = $5
    suma_coord_y = $6
    suma_coord_z = $7
    actual = ""
    # Contador
    n = 0
    flag = 0
}

{
    # Identificacion: ligando, número y cadena
    otro_ligando=($4$3)

    # Diferenciar iones de ligandos
    # Comprueba que se siga contando el mismo ligando, si ya se en-
    # cuentra en otro, se guardan los resultados del ligando anterior
    if (actual != otro_ligando && flag == 0){
        # Linea 1 es especial como no
        if (NR == 1){
            suma_coord_x = 0
            suma_coord_y = 0
            suma_coord_z = 0
            flag = 1
            actual = otro_ligando
            n = 0
        }
        else{
            # Se imprime el centro del ligando, que es la suma de sus coor-
            # denadas, divididas en la cantidad de átomos (contador)
            suma_coord_x = (suma_coord_x) / n
            suma_coord_y = (suma_coord_y) / n
            suma_coord_z = (suma_coord_z) / n

            print "CENTROS: " ligando, actual, suma_coord_x, suma_coord_y, suma_coord_z
            # Reiniciar todo 
            suma_coord_x = 0
            suma_coord_y = 0
            suma_coord_z = 0
            flag = 1
            actual = otro_ligando
            n = 0
        }
    }
    # Imprimir la info analizada de cada uno
    print $2 " " actual " " $5 " " $6 " " $7
    suma_coord_x = (suma_coord_x + $5)
    suma_coord_y = (suma_coord_y + $6)
    suma_coord_z = (suma_coord_z + $7)
    flag = 0
    ligando = $2
    actual = otro_ligando
    n = n + 1

}

END {
    # Centro geometrico para cada ligando
    suma_coord_x = (suma_coord_x) / n
    suma_coord_y = (suma_coord_y) / n
    suma_coord_z = (suma_coord_z) / n
    print "CENTROS: " ligando, actual, suma_coord_x, suma_coord_y, suma_coord_z
}
