#!/bin/bash


# [PARTE 3]
# Analisis de ID
function analisis {
    CARPETA="./PDB_Downloads/"

    # No existe carpeta de descargas
    if [[ ! -d "$CARPETA" ]]; then
        # Crear carpeta
        mkdir "$CARPETA"
    fi

    # Convertir ID a mayúsculas
    ID_a=$(echo $ID |tr ‘[a-z]’ ‘[A-Z]’)
    echo "ID introducida: $ID_a"
    # Buscar coincidencia en Database
    buscarID_a=`grep -w "\$ID_a\b" $DATABASE`
    #echo $buscarID_a

    # Cambia "," que separan los datos por "¬" 
    buscarID_a="${buscarID_a//\"\,\"/¬}"    
    # Elimina las comillas
    buscarID_a="${buscarID_a//\"/}"
    # Convirte busqueda en arreglo
    IFS='¬' read -a array <<< "$buscarID_a"
    name=$(echo "${array[0]}")
    proteina=$(echo "${array[3]}")
    # echo $name
    # echo $proteina

    # Confirmacion de que encuentra el ID
    if [[ "$ID_a" == "$name" ]]; then
        echo 
        echo "       -----------Archivo Identificado-----------       "
        sleep 1
        echo "Información: $buscarID_a"
        echo

        # ID es proteina, descargar
        if [[ "$proteina" == "Protein" ]]; then
            echo "Archivo corresponde a una proteína"
            echo

            # Descarga archivo .pdb
            if [[ ! -f "$CARPETA/$ID_a.pdb" ]]; then            	
                echo "       -----------Descargando Proteina.pdb------------       "
                wget https://files.rcsb.org/download/$ID_a.pdb -P "$CARPETA"
                # Internet OK
                    if ping -c 1 google.com; then
                    echo "       -----------Descarga Completa-----------       "
                    sleep 1
                    # ENVIAR AL OTRO BASH
                    bash proyecto1-20.sh $ID_a
                    exit 0
                    # No Internet
                    else
                        echo
                        echo "No existe conexión a Internet"
                        echo "Por favor, inténtelo más tarde. Adiós"
                        exit 1
                    fi

            # Ya existe el archivo, no descargar nuevamente
            else
               echo "       -----------Proteina Previamente Descargada------------       "
                sleep 1
                echo "Comezando analisis"

                # ENVIAR A EL OTRO BASH o resultado
                bash proyecto1-20.sh $ID_a
            fi

        # No es proteina, no decarga
        else
            echo "ERROR"
            echo "Imposible descargar el documento"
            echo "Archivo no corresponde a una proteína"
            echo
            parametros
            exit 1
        fi

    # Archivo invalido
    else
        echo 
        echo "ERROR"
        echo "Archivo no existe"
        echo 
        parametros
        exit 1
    fi
}


# [PARTE 2]
# Revision cantidad de parámetros
function parametros {
    DATABASE=bd-pdb.txt
    # Contar parámetros ingresados
    read -p "Ingrese PDB ID:" ID
    cantidad_parametros=$(echo $ID | wc -w)
    # echo "ID tiene $cantidad_parametros palabras"

    # Revisar si el argumento es alfanumérico y de 4 dígitos
    if ! [[ "$ID" =~ ^[A-Za-z0-9]{4,}$ ]]; then
        echo
        echo "ERROR"
        echo "Se necesita un argumento válido (4 dígitos)"
        parametros
        exit 1
    fi

    # Estandar: 1 parámetro
    if [[ "$cantidad_parametros" -eq "1" ]]; then
    # Continuar..
        # Revision DATABASE
        # No existe DATABASE
        if [[ ! -f $DATABASE ]]; then
            echo "       -----------ADVERTENCIA-----------       "
            echo "Base de datos no encontrada"
            sleep 1
            echo "Comprobando conexión a internet"
            echo
            # Internet OK
            if ping -c 1 google.com; then
                # echo "INTERNET OK!"
                echo "       -----------Descargando base de datos------------       "
                wget https://icb.utalca.cl/~fduran/bd-pdb.txt
                echo "       -----------Base de datos descargada-----------       "
                echo
                # Analisis de ID en DATABASE
                analisis $ID $DATABASE
                exit 0
            else
                echo "No existe conexión a Internet"
                echo "Por favor, inténtelo más tarde. Adiós"
                exit 1
            fi
        # Existe DATABASE
        else
            # Analisis de ID en DATABASE
            analisis $ID $DATABASE
            exit 0
        fi

    # No ingresan parámetro
    elif [[ "$cantidad_parametros" -eq "" ]]; then
        # Error
        echo
        echo "ERROR" 
        echo "parámetro no ingresado"
        parametros
        exit 1

    # Más de 1 parámetro
    else
        echo
        echo "ERROR"
        echo "Se ingresaron $cantidad_parametros parámetros"
        echo "Ingrese sólo UN parámetro"
        echo
        parametros
        exit 1
    fi
}


# MAIN
# [PARTE 1] INICIO PROGRAMA 
function main {

    echo "       -----------Bienvenido-----------       "
    echo
    echo "¿Qué desea hacer?"
    echo "1.- Buscar archivo PDB y generar imagen"
    echo "2.- Informacion sobre el programa"
    echo "3.- Salir"
    echo ""

    read opcion

    if [[ $opcion == "1" ]]; then
        # Enviar a otra funcion para analisis 
        # de parámetros ingresados (cantidad)
        parametros

    elif [[ $opcion == "2" ]]; then

        echo "       -----------INFORMACION-----------       "
        echo ""
        echo "Programa diseñado para procesar datos obtenidos"
        echo "de un archivo PDB a partir de una base de datos"
        echo "predeterminada."
        echo "Se procesaran ficheros EXCLUSIVAMENTE del tipo "
        echo "proteína, demás archivos no serán descargados  "
        echo "ni procesados."
        echo "Los resultados se archivaran en la carpeta "
        echo "'PDB_Downloads/' en forma de imagenes con forma-"
        echo "to PNG."
        echo ""
        echo "¿Desea volver al menu principal? Y/N"
        echo ""
        read opcion2
        case $opcion2 in
            y|Y) 
            echo "Usted pulsó la opción SI"
            clear
            main
            ;;
            n|N)
            echo "Usted pulsó la opción NO"
            sleep 2
            echo "Fin del programa"
            echo "Adiós"
            exit 0
            ;;
            *)
            echo "Opción invalida"
            echo "Adiós"
            exit 0
            ;;
        esac

    elif [[ $opcion == "3" ]]; then
        echo "Adiós"
        exit 0

    else
        echo "Opción inválida"
        sleep 1
        clear
        main
    fi
}

main