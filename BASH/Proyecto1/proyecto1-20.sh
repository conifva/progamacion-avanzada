#!/bin/bash

codigo="$1"
CARPETA="./PDB_Downloads/"
cd "$CARPETA"

# Analisis $ID_a.pdb, ATOM
echo
echo "       -----------Procesando Datos----------       "
echo "Puede tardar unos minutos"
grep ^"ATOM" "$codigo".pdb > "resumen_$1".txt
# .txt contiene lo necesario para empezar el analisis, como: atom(1), elemento(3), aa(4), cadena(5), numero aa(6)
cat "$codigo".pdb |grep ^ATOM |awk '{print (substr($1,1,4) " " substr($3,1,3) " " substr($0,18,20))}' > "resumen_$1".txt
echo
#awk '{if ($1 == "ATOM") {print $1 " " $3 " " $4 " " $5 }}' > "resumen_$1".txt
# cadena, aa
##awk '{print $4, $3}' "resumen_$1".txt | sort | uniq > "prueba_aa$1".dot
# Copiar archivo txt fuera de la carpeta para que sea analizado por awk
cp "resumen_$1".txt $PWD/../
cd $PWD/../
# Transformar txt a dot luego del analisis por awk
cat "resumen_$1".txt | awk -f pdb_a_dot.awk >> "$codigo".dot
# Eliminar archivo txt copiado
rm "resumen_$1".txt
echo "       -----------Generando Imagen----------       "
echo "Puede tardar unos minutos"
# Tranformar dot a formato png
dot "$codigo".dot -o "$codigo".png -Tpng -Gcharset=utf8
mv "$codigo".dot $CARPETA
# Se creó la imagen y se movio a la carpeta
echo
echo "       -----------Imagen generada con éxito----------      "
mv "$codigo".png $CARPETA
echo "Nombre del archivo: $codigo.png"
echo "Ingrese a la carpeta "$CARPETA" para ver el gráfico"
echo "Adiós"
exit 0
