BEGIN{

    # Auxiliar para imprimir " 
    # Inicia gráfico
    x="\""
    print "digraph G {"
}

{
    actual=$3$4$5

    # Revisar si aa actual es igual al anterior
    if(actual != anterior){

        # Distintos entonces se grafica
        print ""
        print "node [shape=tripleoctagon]"
        print actual " [label = "x$3x"]"

        # Misma cadena se unen
        if ($4 == cadena_anterior){
            print actual " -> " anterior "[label = Cadena"$4"]"
        }

        print ""
        print "node [shape=circle]"
    }

    # Se crea nodo por cada elemento diferenciando aminoacidos con el mismo
    # nombre pero con diferente número de residuo (ambos de una misma cadena)
    print $2$4$5 " [label = "x$2x"]"
    print $2$4$5 " -> " actual "[arrowhead = vee]"

    # Comparación de lineas
    anterior=actual
    cadena_anterior=$4
}

END{
    
    # Cerrar el gráfico
    print "}"
}
