package com.example.convertidor2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    /*
    PASO 0 EN "arrays.xml"
    <!--- crear elementos del array
            (a los elementos los ponemos en un identificador llamado "monedas")  -->

    PASO 1
    arreglo con denominacion de monedas
    llanar un spinner con un arrey adapter
    */
    final String[] datos=new String[] {"DOLAR", "PESOS CHILENOS"};

    //PASO 2
    //VARIABLE QUE MANEJA CONTROLES
    //una variable para cada control
    private Spinner spinner_monedaActual;
    private Spinner spinner_monedaCambio;
    private EditText editText_valorCambio;
    private TextView textView_resultado;

    //PASO 3
    //Definir factores de conversion, basado en:
    //1 dolar ---> 680 pesos chile
    //1 peso chile ---> 0.0015 dolar aprox
    final private double factorDolarPeso = 680;
    final private double factorPesoDolar = 0.0015;

    //opcoin para capturar el spiner:
    //--> final Spinner spinner = (Spinner) findViewById(R.id.spinner);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //PASO 4
        //ArrayAdapter
        //agregar los elementos del array dato al spinner spinner_MonedaActual
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,R.layout.support_simple_spinner_dropdown_item,datos);

        spinner_monedaActual = (Spinner) findViewById(R.id.spinner_monedaActual);

        spinner_monedaActual.setAdapter(adaptador);
    }

    //PASO 5
    //agregar evento click, se enlaza con el boton
    public void clickConvertir(View view){
        //PASO 6
        //Obtener los controles
        //los controles nos informan que elementos de moneda estan seleccionados
        spinner_monedaActual = (Spinner) findViewById(R.id.spinner_monedaActual);
        spinner_monedaCambio = (Spinner) findViewById(R.id.spinner_monedaCambio);
        //obtener el control ValorCambio
        // el control contiene la informacion del valor de cambio
        editText_valorCambio = (EditText) findViewById(R.id.editText_valorCambio);
        // y textView_Resultado
        //permite enviar mensajes al usuario en la interfaz
        textView_resultado = (TextView) findViewById(R.id.textView_resultado);

        //PASO 7
        //spinner_MonedaActual.getSelectedItem()
        //obtine el elemento seleccionado del spinner
        String monedaActual = spinner_monedaActual.getSelectedItem().toString();
        String monedaCambio = spinner_monedaCambio.getSelectedItem().toString();

        //PASO 8
        //obtenemos el valor a cambiar
        //Double.parseDouble([STRING]) permite convertir un String a Double
        double valorCambio = Double.parseDouble(editText_valorCambio.getText().toString());

        //procesarConversion,  metodo con la logica para procesar la conversion, recibe 3 parametros
        double resultado= procesarConversion(monedaActual, monedaCambio, valorCambio);

        //PASO 10
        //si procesarCambio devuelve un valor > 0,
        // el cambio fue procesado correctamente
        if(resultado>0){
            //%5.2f corresponde a val1 - una variable double/float de 5 enteros 2 decimales
            textView_resultado.setText(String.format("Por %5.2f %s, usted recibirá %5.2f %s",valorCambio,monedaActual,resultado,monedaCambio));
            //editText_ValorCambio.setText("")
            //borrar cualquier valor del EditText valorCambio
            editText_valorCambio.setText("");
        }else{
            //si procesarCambio devuelve un valor <= 0,
            //informar al usuario que ocurrio un problema en la conversion
            textView_resultado.setText(String.format("Usted recibirá"));
            Toast.makeText(MainActivity.this, "Las opciones elegidas no tienen un factor de conversion",Toast.LENGTH_SHORT).show();
        }
    }

    //PASO 9
    //metodo con la logica para las conversiones
    //recibe 3 parametros monedaActual, monedaCambio, valorCambio
    private double procesarConversion(String monedaActual, String monedaCambio, double valorCambio){
        //PASO 11
        //Implementar el metodo
        double resultadoConversion = 0;

        //Evaluar monedaActual
        // opciones posibles: que sea dolar o pesos chilenos
        switch (monedaActual){
            case "DOLAR":
                if (monedaCambio.equals("PESOS CHILENOS"))
                    resultadoConversion = valorCambio * factorDolarPeso;

                if (monedaCambio.equals("DOLAR"))
                    resultadoConversion = valorCambio;

                break;
            case "PESOS CHILENOS":
                if (monedaCambio.equals("DOLAR"))
                    resultadoConversion = valorCambio * factorPesoDolar;

                if (monedaCambio.equals("PESOS CHILENOS"))
                    resultadoConversion = valorCambio;

                break;

        }
        return resultadoConversion;
    }
}
